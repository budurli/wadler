import types


class MethodControlMixin(object):
    @classmethod
    def addMethod(cls, func):
        return setattr(cls, func.__name__, types.MethodType(func, cls))

    @classmethod
    def removeMethod(cls, name):
        return delattr(cls, name)


class FunctionFabric(object):
    def __new__(cls, *args, **kwargs):
        pass
