# -*- coding: utf-8 -*-
import types

import requests

from .parser import WADLParser
from .fun_generator import MethodControlMixin


test = requests.get('http://www.google.com')
if 200 != test.status:
    raise Exception('No internet connection')


class BaseService(MethodControlMixin):
    def __init__(self, base_url, username=None, password=None):
        self.parser = WADLParser(base_url, username, password)
        self.resources = self.parser.resources
        self.create_methods()

    def create_methods(self):
        for resource in self.resources:
            self._create_method(
                name=resource.id,
                method=resource.type,
                path=resource.path
            )

    def _create_method(self, name, method, path):
        function_pattern = u"""
        def _fun_template(self):
            r = requests.%(method)s("%(path)s")
            return r.json()
        """ % dict(method=method, path=path)
        template = compile(function_pattern, 'base.py', 'exec')
        self.addMethod(types.FunctionType(template, name))



