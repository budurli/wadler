# -*- coding: utf-8 -*-
import requests

import lxml.etree as le


class WADLParser(object):
    base_url = ''
    credentials = ''
    resources = []

    def __init__(self, base_url, username=None, password=None):
        self.base_url = base_url
        self.credentials = (username, password)
        self.filename = 'application.wadl'

        self.file = open(self.filename, 'w')  # opening file for writing
        r = requests.get(self.base_url + 'application.wadl', auth=self.credentials)
        self.file.write(r.text)
        self.file.close()

        self.tree = le.parse(self.filename)
        self.root = self.tree.getroot()
        self.ns = self.root.nsmap[None]  # yeah, really

        self.resources_el = self.root.find(u'{%s}resources' % self.ns)
        self.resource_el = u'{%s}resource' % self.ns
        self.method_el = u'{%s}method' % self.ns

    def _get_fractal_path(self, element, root_path=''):
        child_resources = element.findall(self.resource_el)
        this_methods = element.findall(self.method_el)

        this_path = str(element.get('path'))

        if not this_path.startswith('/') and not root_path.endswith('/'):
            this_path = '/' + this_path

        if this_methods:
            self.resources.append([
                {
                    'path': this_path,
                    'type': method.get('name'),
                    'id': method.get('id')
                }
                for method in this_methods])

        for el in child_resources:
            self._get_fractal_path(el, root_path=this_path)

    def get_resources(self):
        self._get_fractal_path(element=self.resources_el)
        return self.resources